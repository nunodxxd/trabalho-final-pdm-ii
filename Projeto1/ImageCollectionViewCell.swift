//
//  ImageCollectionViewCell.swift
//  Projeto1
//
//  Created by Nuno  Baptista on 19/07/18.
//  Copyright © 2018 Nuno  Baptista. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView : UIImageView!
    
}
