//
//  GridViewController.swift
//  Projeto1
//
//  Created by Nuno  Baptista on 19/07/18.
//  Copyright © 2018 Nuno  Baptista. All rights reserved.
//

import UIKit

class GridViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   
    @IBOutlet weak var collectionview: UICollectionView!
    
    var gridLayout: GridLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gridLayout = GridLayout(numberOfColumns: 3)
        collectionview.collectionViewLayout = gridLayout
        collectionview.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contatos.sharedInstance.Lista.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as! ImageCollectionViewCell
        
        let image = contatos.sharedInstance.ListaImg[indexPath.row]
        cell.imageView.image = image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        myindex = indexPath.row
        performSegue(withIdentifier: "segue", sender: self)
    }
}
