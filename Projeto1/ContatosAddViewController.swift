//
//  ContatosAddViewController.swift
//  Projeto1
//
//  Created by Nuno  Baptista on 21/06/18.
//  Copyright © 2018 Nuno  Baptista. All rights reserved.
//

import UIKit

class ContatosAddViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var nome:String = ""
    var numero:String = ""
    var img:UIImage = #imageLiteral(resourceName: "noImg_2")
    
    @IBOutlet weak var Nome: UITextField!
    @IBOutlet weak var imagePicked: UIImageView!
    @IBOutlet weak var num: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Adicionar(_ sender: Any) {
        if(Nome.text != "" && num.text != ""){
        nome = Nome.text!
        numero = num.text!
        contatos.sharedInstance.Lista.append(nome)
        contatos.sharedInstance.ListaImg.append(img)
        contatos.sharedInstance.ListaNum.append(numero)
        }
        
    }
    
    @IBAction func escolherFoto(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Fonte da Foto", message: "Escolha uma fonte", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController,animated: true, completion: nil)
            }else{
                print("Camera nao disponivel")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Biblioteca", style: .default, handler: { (action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController,animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true,completion: nil)
       
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicked.image = image
        img = cropToBounds(image: image, width: 200, height: 200)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //funcao para recortar a imagem 
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
}
