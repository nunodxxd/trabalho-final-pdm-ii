//
//  ContactosTableViewController.swift
//  Projeto1
//
//  Created by Nuno  Baptista on 21/06/18.
//  Copyright © 2018 Nuno  Baptista. All rights reserved.
//

import UIKit

var myindex = 0

class contatos {
    static let sharedInstance = contatos()
    
     var Lista = [String]()
     var ListaNum = [String]()
     var ListaImg = [UIImage]()

    private init() { }
}


class ContactosTableViewController: UITableViewController {
    
    
    override func awakeFromNib() {
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contatos.sharedInstance.Lista.count
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContatosCell", for: indexPath)
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "ContatosCell2", for: indexPath )
        
        //imagem redonda
        cell.imageView?.layer.cornerRadius = (cell.imageView?.frame.size.width)!/2;
        cell.imageView?.layer.masksToBounds = true;
        cell.imageView?.layer.borderWidth = 0;

        
        if(contatos.sharedInstance.ListaImg[indexPath.row] != #imageLiteral(resourceName: "noImg_2"))
        {
        cell.textLabel!.text = contatos.sharedInstance.Lista[indexPath.row]
        cell.imageView?.image = contatos.sharedInstance.ListaImg[indexPath.row]
        cell.detailTextLabel?.text = contatos.sharedInstance.ListaNum[indexPath.row]
            
            return cell
        }
        else
        {
        cell2.textLabel!.text = contatos.sharedInstance.Lista[indexPath.row]
        cell2.detailTextLabel?.text = contatos.sharedInstance.ListaNum[indexPath.row]
            return cell2
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myindex = indexPath.row
        performSegue(withIdentifier: "segue", sender: self)
        
    }

}
