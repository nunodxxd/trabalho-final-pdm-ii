//
//  ContactosDetalhes.swift
//  Projeto1
//
//  Created by Nuno  Baptista on 12/07/18.
//  Copyright © 2018 Nuno  Baptista. All rights reserved.
//

import UIKit

class ContactosDetalhes: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nome.text = contatos.sharedInstance.Lista[myindex]
        numero.text = contatos.sharedInstance.ListaNum[myindex]
        imagem.image = contatos.sharedInstance.ListaImg[myindex]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    

}
